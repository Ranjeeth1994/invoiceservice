package com.demo.trimind.invoice.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PaymentTerms")
public class PaymentTermsEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="Code")
	private String code;
	
	@Column(name="Description")
	private String description;
	
	@Column(name="CreationDate")
	private LocalDate creationDate = LocalDate.now();
	
	@Column(name="Days")
	private Integer days;
	
	@Column(name="RemindBeforeDays")
	private Integer remindBeforeDays;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}

	public Integer getRemindBeforeDays() {
		return remindBeforeDays;
	}

	public void setRemindBeforeDays(Integer remindBeforeDays) {
		this.remindBeforeDays = remindBeforeDays;
	}

	@Override
	public String toString() {
		return "PaymentTermsEntity [id=" + id + ", code=" + code + ", description=" + description + ", creationDate="
				+ creationDate + ", days=" + days + ", remindBeforeDays=" + remindBeforeDays + "]";
	}
	
	

}