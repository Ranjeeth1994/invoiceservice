package com.demo.trimind.invoice.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Invoice")
public class InvoiceEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private Integer id;
	
	@Column(name="InvoiceNumber")
	private String invoiceNumber;
	
	@Column(name="InvoiceDate")
	private LocalDate invoiceDate;
	
	
	/*
	 * @Column(name="PaymentTerm") private String paymentTerm;
	 */
	
	@Column(name="Status")
	private String status;

	
	@ManyToOne
	@JoinColumn(name="PaymentTerm")
	PaymentTermsEntity paymentTerms;

	public PaymentTermsEntity getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(PaymentTermsEntity paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public LocalDate getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(LocalDate invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	/*
	 * public String getPaymentTerm() { return paymentTerm; }
	 * 
	 * public void setPaymentTerm(String paymentTerm) { this.paymentTerm =
	 * paymentTerm; }
	 */

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "InvoiceEntity [id=" + id + ", invoiceNumber=" + invoiceNumber + ", invoiceDate=" + invoiceDate
				 + ", status=" + status + ", paymentTerms=" + paymentTerms + "]";
	}

}
