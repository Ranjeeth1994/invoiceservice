package com.demo.trimind.invoice.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.demo.trimind.invoice.entity.InvoiceEntity;
import com.demo.trimind.invoice.repository.InvoiceRepository;

@Component
public class CronInvoiceService {
	
	
    private InvoiceRepository repository;
	
	public CronInvoiceService(InvoiceRepository repository) {
		this.repository = repository;
	}

	
	// Run at 12 am every day
	@Scheduled(cron="0 0 * * * ?")
	public void sendInvoiceRemainders()
	{
		List<InvoiceEntity> invoiceList = repository.findByStatus("UNPAID");
		for(InvoiceEntity invoice: invoiceList){
			LocalDate invoiceDate = invoice.getInvoiceDate();
			
			Integer days = invoice.getPaymentTerms().getDays();
			Integer remindBeforeDays = invoice.getPaymentTerms().getRemindBeforeDays();
			//System.out.println("Invoice Details " + invoice.toString() + " Payment Terms Details " + invoice.getPaymentTerms().toString());
			
			
			LocalDate reminderDate = invoiceDate.plusDays( days - remindBeforeDays);
			//System.out.println("Remainder Date " + reminderDate + " Current Date " + LocalDate.now());
			//System.out.println(" Date condition is " + reminderDate.isEqual(LocalDate.now()));
			
			if( reminderDate.isEqual(LocalDate.now()))
			{
				System.out.println("Sending the remainder for the invoice " + invoice.getInvoiceNumber());
			}
		}
	}
	
	

}