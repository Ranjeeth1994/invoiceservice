package com.demo.trimind.invoice.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.demo.trimind.invoice.entity.InvoiceEntity;
import com.demo.trimind.invoice.repository.InvoiceRepository;

@Service
public class InvoiceService {
	
	private InvoiceRepository repository;
	
	public InvoiceService(InvoiceRepository repository) {
		this.repository = repository;
	}
	
	public void createInvoice(List<InvoiceEntity> invoiceSet)
	{
		repository.saveAll(invoiceSet);
	}
	
	public List<InvoiceEntity> fetchInvoice()
	{
		return repository.findAll();
	}

}
