package com.demo.trimind.invoice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.demo.trimind.invoice.entity.InvoiceEntity;

public interface InvoiceRepository extends JpaRepository<InvoiceEntity,Integer> {


 // INVOICENUMBER I, INVOICEDATE I, DAYS PTS, REMINDBEFOREDAYS PTS
	@Query("select i from InvoiceEntity i where i.status=:status")
	public List<InvoiceEntity> findByStatus(@Param("status") String status);

}
