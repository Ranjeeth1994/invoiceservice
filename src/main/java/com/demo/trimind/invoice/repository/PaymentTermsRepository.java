package com.demo.trimind.invoice.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.demo.trimind.invoice.entity.PaymentTermsEntity;

public interface PaymentTermsRepository extends JpaRepository<PaymentTermsEntity, Integer>{
	
	
     @Query("select t from PaymentTermsEntity t where t.code =:code")
     public PaymentTermsEntity getPaymentTermsByCode(@Param("code") String code);

}