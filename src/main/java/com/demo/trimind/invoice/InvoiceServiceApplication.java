package com.demo.trimind.invoice;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.demo.trimind.invoice.entity.InvoiceEntity;
import com.demo.trimind.invoice.entity.PaymentTermsEntity;
import com.demo.trimind.invoice.repository.PaymentTermsRepository;
import com.demo.trimind.invoice.service.InvoiceService;

@SpringBootApplication
@EnableScheduling
public class InvoiceServiceApplication implements CommandLineRunner {
	
	@Autowired
	private InvoiceService service;
	
	@Autowired
	private PaymentTermsRepository paymentRepository;

	public static void main(String[] args) {
		SpringApplication.run(InvoiceServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception 
	{
		
		PaymentTermsEntity net30 = paymentRepository.getPaymentTermsByCode("NET 30");
		PaymentTermsEntity net45 = paymentRepository.getPaymentTermsByCode("NET 45");
		PaymentTermsEntity net15 = paymentRepository.getPaymentTermsByCode("NET 15");
		
		InvoiceEntity entity1 = new InvoiceEntity();
		entity1.setInvoiceNumber("INV-001");
		entity1.setInvoiceDate(LocalDate.parse("2021-01-10"));
		 entity1.setPaymentTerms(net30);
		entity1.setStatus("UNPAID");
		
		InvoiceEntity entity2 = new InvoiceEntity();
		entity2.setInvoiceNumber("INV-002");
		entity2.setInvoiceDate(LocalDate.parse("2021-10-21"));
		entity2.setPaymentTerms(net45);
		entity2.setStatus("PAID");
		
		InvoiceEntity entity3 = new InvoiceEntity();
		entity3.setInvoiceNumber("INV-003");
		entity3.setInvoiceDate(LocalDate.parse("2021-01-11"));
		entity3.setPaymentTerms(net30);
		entity3.setStatus("UNPAID");
		
		InvoiceEntity entity4 = new InvoiceEntity();
		entity4.setInvoiceNumber("INV-004");
		entity4.setInvoiceDate(LocalDate.parse("2021-11-24"));
		
		entity4.setPaymentTerms(net15);
		entity4.setStatus("UNPAID");
		
		List<InvoiceEntity> entities = new ArrayList();
		entities.add(entity1);
		entities.add(entity2);
		entities.add(entity3);
		entities.add(entity4);
		
		service.createInvoice(entities);
		
		
		entity1.getInvoiceDate();
		
	}

}
