DROP TABLE IF EXISTS Invoice;

CREATE TABLE Invoice(
Id int AUTO_INCREMENT,
InvoiceNumber varchar2(30) NOT NULL,
InvoiceDate DATE,
PaymentTerm varchar2(30),
Status varchar2(30)
FOREIGN KEY (PaymentTerm) REFERENCES Payment_Terms (code));